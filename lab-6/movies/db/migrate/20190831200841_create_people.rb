class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.text :first_name
      t.text :last_name
      t.numeric :country_id
      t.timestamps
    end
  end
end
