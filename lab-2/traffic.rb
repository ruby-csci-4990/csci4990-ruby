# Ben Rongey
# CSCI 4990
# Summer 2019

require 'csv'
require 'haversine'
require 'geocoder'

query = ARGV[0]
puts "looking up: #{query}"

# Uses the Geocoder gem to look up the lattitude and longitude of the given ARG
query_data = Geocoder.search(query)
@lat = query_data.first.latitude
@long = query_data.first.longitude

# Creation of necessary variables and arrays
location = ''
final_distance = 0
distances = Array.new
locations = Array.new
i = 0

# CSV for loop that will loop through each row of the CSV
CSV.foreach("./cameras.csv") do |row|
  if i == 0
    i += 1
    next
  end

  # Uses the Haversine gem to calculate the distance via the haversine formula, then converts to miles
  temp_distance = Haversine.distance(@lat, @long, row[1].to_f, row[2].to_f).to_miles

  # Stores distances and locations in arrays
  distances << temp_distance
  locations << row[0]

  # Calculates the smallest distance and the corresponding location
  final_distance = distances.min
  location = locations[distances.index(distances.min)]

  i += 1

end

# Final output prints the required statement
puts "The closest functional camera is located at #{location} and it's  #{final_distance} miles away!"

