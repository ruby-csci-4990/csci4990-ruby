# == Schema Information
# Schema version: 20190731213625
#
# Table name: traffic_cameras
#
#  id             :bigint           not null, primary key
#  name           :text
#  latitude       :decimal(, )
#  longitude      :decimal(, )
#  is_operational :boolean
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

require 'haversine'
require 'geocoder'

class TrafficCamera < ApplicationRecord
  validates :id, :name, :latitude, :longitude, :is_operational, presence: true
  validates :name, uniqueness: true
  validates :latitude, :inclusion => -90...90
  validates :longitude, :inclusion => -180...180

  def distance_to(lat, lon)

    Haversine.distance(lat, lon, self.latitude, self.longitude).to_miles

  end

  def self.closest_active_camera(lat, lon)
    distances = []

    TrafficCamera.all.each do |traffic_camera|
      distance = traffic_camera.distance_to(lat, lon)
      distances << distance
    end

    TrafficCamera.all.map{|c| [c, c.distance_to(lat,lon)] }.min_by{|v| v[1]}[0]
  end
end
