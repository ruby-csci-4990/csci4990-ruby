class TrafficCamerasController < ApplicationController
  before_action :set_traffic_camera, only: [:show, :edit, :update, :destroy]

  # GET /traffic_cameras
  # GET /traffic_cameras.json
  def index
    @traffic_cameras = TrafficCamera.all
  end

  # GET /traffic_cameras/1
  # GET /traffic_cameras/1.json
  def show
  end

  # GET /traffic_cameras/new
  def new
    @traffic_camera = TrafficCamera.new
  end

  # GET /traffic_cameras/1/edit
  def edit
  end

  # POST /traffic_cameras
  # POST /traffic_cameras.json
  def create
    @traffic_camera = TrafficCamera.new(traffic_camera_params)

    respond_to do |format|
      if @traffic_camera.save
        format.html { redirect_to @traffic_camera, notice: 'Traffic camera was successfully created.' }
        format.json { render :show, status: :created, location: @traffic_camera }
      else
        format.html { render :new }
        format.json { render json: @traffic_camera.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /traffic_cameras/1
  # PATCH/PUT /traffic_cameras/1.json
  def update
    respond_to do |format|
      if @traffic_camera.update(traffic_camera_params)
        format.html { redirect_to @traffic_camera, notice: 'Traffic camera was successfully updated.' }
        format.json { render :show, status: :ok, location: @traffic_camera }
      else
        format.html { render :edit }
        format.json { render json: @traffic_camera.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /traffic_cameras/1
  # DELETE /traffic_cameras/1.json
  def destroy
    @traffic_camera.destroy
    respond_to do |format|
      format.html { redirect_to traffic_cameras_url, notice: 'Traffic camera was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_traffic_camera
      @traffic_camera = TrafficCamera.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def traffic_camera_params
      params.require(:traffic_camera).permit(:latitude, :longitude, :name, :is_operational)
    end
end
