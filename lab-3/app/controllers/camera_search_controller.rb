# require 'traffic_module'

# this comment only exists to satisfy the linter
class CameraSearchController < ApplicationController
  def new
    # Nothing to do here.
  end

  def results
    result = Geocoder.search(params[:query]).first
    if result.nil?
      # Geocoding was not successful.
      @traffic_camera = nil
      @distance = nil
    else
      data = result.data
      query_latitude = data['lat'].to_f; query_longitude = data['lon'].to_f
      @traffic_camera = TrafficCamera.closest_active_camera(query_latitude,query_longitude)
      @distance = @traffic_camera.distance_to(query_latitude,query_longitude)
    end
  end

end