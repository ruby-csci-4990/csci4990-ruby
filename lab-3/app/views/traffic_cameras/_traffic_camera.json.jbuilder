json.extract! traffic_camera, :id, :latitude, :longitude, :name, :is_operational, :created_at, :updated_at
json.url traffic_camera_url(traffic_camera, format: :json)
