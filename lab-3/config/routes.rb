
Rails.application.routes.draw do

  scope :traffic, :controller => :camera_search do

    get :new, :as => :new_camera_search
    get :results, :as => :camera_search_results

    resources :traffic_cameras

  end
end