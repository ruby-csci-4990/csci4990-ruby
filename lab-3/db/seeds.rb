# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

CSV.foreach(Rails.root.join('lib', 'cameras.csv')) do |row|
  # if i == 0
  #   i += 1
  #   next
  # end

  if row[0] != 'Location'
    s = TrafficCamera.new(name: row[0],
                          latitude: row[1],
                          longitude: row[2],
                          is_operational: true)

    s.save
  end


  # i += 1

end