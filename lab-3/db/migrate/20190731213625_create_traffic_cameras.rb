class CreateTrafficCameras < ActiveRecord::Migration[5.2]
  def change
    create_table :traffic_cameras do |t|
      t.text :name
      t.numeric :latitude
      t.numeric :longitude
      t.boolean :is_operational
      t.timestamps
    end
  end
end
