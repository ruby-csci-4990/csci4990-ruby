require "application_system_test_case"

class TrafficCamerasTest < ApplicationSystemTestCase
  setup do
    @traffic_camera = traffic_cameras(:one)
  end

  test "visiting the index" do
    visit traffic_cameras_url
    assert_selector "h1", text: "Traffic Cameras"
  end

  test "creating a Traffic camera" do
    visit traffic_cameras_url
    click_on "New Traffic Camera"

    check "Is operational" if @traffic_camera.is_operational
    fill_in "Latitude", with: @traffic_camera.latitude
    fill_in "Longitude", with: @traffic_camera.longitude
    fill_in "Name", with: @traffic_camera.name
    click_on "Create Traffic camera"

    assert_text "Traffic camera was successfully created"
    click_on "Back"
  end

  test "updating a Traffic camera" do
    visit traffic_cameras_url
    click_on "Edit", match: :first

    check "Is operational" if @traffic_camera.is_operational
    fill_in "Latitude", with: @traffic_camera.latitude
    fill_in "Longitude", with: @traffic_camera.longitude
    fill_in "Name", with: @traffic_camera.name
    click_on "Update Traffic camera"

    assert_text "Traffic camera was successfully updated"
    click_on "Back"
  end

  test "destroying a Traffic camera" do
    visit traffic_cameras_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Traffic camera was successfully destroyed"
  end
end
