require 'test_helper'

class TrafficCamerasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @traffic_camera = traffic_cameras(:one)
  end

  test "should get index" do
    get traffic_cameras_url
    assert_response :success
  end

  test "should get new" do
    get new_traffic_camera_url
    assert_response :success
  end

  test "should create traffic_camera" do
    assert_difference('TrafficCamera.count') do
      post traffic_cameras_url, params: { traffic_camera: { is_operational: @traffic_camera.is_operational, latitude: @traffic_camera.latitude, longitude: @traffic_camera.longitude, name: @traffic_camera.name } }
    end

    assert_redirected_to traffic_camera_url(TrafficCamera.last)
  end

  test "should show traffic_camera" do
    get traffic_camera_url(@traffic_camera)
    assert_response :success
  end

  test "should get edit" do
    get edit_traffic_camera_url(@traffic_camera)
    assert_response :success
  end

  test "should update traffic_camera" do
    patch traffic_camera_url(@traffic_camera), params: { traffic_camera: { is_operational: @traffic_camera.is_operational, latitude: @traffic_camera.latitude, longitude: @traffic_camera.longitude, name: @traffic_camera.name } }
    assert_redirected_to traffic_camera_url(@traffic_camera)
  end

  test "should destroy traffic_camera" do
    assert_difference('TrafficCamera.count', -1) do
      delete traffic_camera_url(@traffic_camera)
    end

    assert_redirected_to traffic_cameras_url
  end
end
